# Eclypsium scan job checker

## Setup

This tool uses a user account to connect to the API server and pull job listings for the given host. As such you would need to create a user account in the web console and retreive the host id

* The user account can be created by logging in as an administrative user and navigating to Administration -> Accounts -> Create Account
* Install prerequisites

       pip install -r requirements.txt

* To get the host id you need to parse it from the stdout of the scanner application. Here are three ways:

1. Parse it after the scan (works for the version of the agent prior to and including 2.1)

       sudo EclypsiumApp -medium | sed -n 's/Scan data updated successfully for host \(\w*\) at domain .*/\1/p'

2. Get the host id without running the scan (works in case the host was persistently registered with `-s2`)

       sudo EclypsiumApp -show-config | sed -n 's/- Host ID: //p'

* Note that with the version 2.2 host deduplication functionality was addeed to the backend, which means that the host ID may change after the job is submitted in case that host is identical to the one that already is registered in the system.
Since version 2.4 the job-id is printed by the agent at the end of the run in this form: `Scan data updated successfully. Check the device at https://.*/resolve-job/(.*)`. The script needs to be updated to query by job-id directly.

## Usage

Once you have the host id run

       waitForScanToComplete.py -u USER -p PASSWORD -s SERVER -i hostID [-t TIMEOUT]

Timeout is set by default to 120

To add logic to act on the results of the scan see the end of the `waitForScanToComplete.py`. You could then build the action logic directly into it or wrap it into a another script that will use return codes

## Notes

In upcoming iterations the tool will be using an API token instead of relying on the username and passwords.
