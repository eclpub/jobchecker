'''
Wait for the Eclypsium scan to complete and report results

Run with -h to see the complete options list
'''
import logging
import sys
import time
import urllib.parse
import itertools
from oauthlib.oauth2 import LegacyApplicationClient
from requests import Response
from requests_oauthlib import OAuth2Session
import pprint
import argparse
import json
from abc import ABC, abstractmethod
from enum import Enum
from typing import Any, Union, List


class DeviceIntegrityInfo:
    def __init__(self, implants: str, devices_checked: str, integrity_warnings: str,
                 firmware_changes: str):
        self.implants = implants
        self.devices_checked = devices_checked
        self.integrity_warnings = integrity_warnings
        self.firmware_changes = firmware_changes

    def __repr__(self):
        return str({k: v for k, v in self.__dict__.items() if not k.startswith('_')})


class SecurityTest:
    def __init__(self, response: dict):
        self.name = response.get('name', None)
        self.result = response.get('result', None)
        self.result_description = response.get('resultDescription', None)
        self.display_name = response.get('displayName', None)
        self.module_description = response.get('moduleDescription', None)

    def __repr__(self):
        return json.dumps({k: v for k, v in self.__dict__.items() if not k.startswith('_')},
                          indent=2)

    @staticmethod
    def __sort(security_test: 'SecurityTest'):
        return security_test.name

    @classmethod
    def array_from_response(cls, response: list) -> List['SecurityTest']:
        array = [SecurityTest(st) for st in response]
        array.sort(key=cls.__sort)
        return array


class DeviceRiskComponent:
    class Status(Enum):
        NON_EXISTENT = 'null'
        WARNING = 'warn'
        THREAT = 'fail'
        PASSED = 'pass'

    def __init__(self, name: str, data: dict):
        self.data = data
        self.name = name
        self.integrity = self._get_integrity(data)
        self.firmware_change = self._get_firmware_change(data)
        self.security_risk = self._get_security_risk(data)
        self.version = self._get_version(data)

    def __str__(self):
        return \
            '"{}": {{"integrity": "{}", "firmware_change": "{}", "security_risk": "{}", "version": "{}"}}'\
            .format(self.name, self.integrity.name, self.firmware_change.name,
                    self.security_risk.name, self.version.name)

    @staticmethod
    def _get_integrity(data: dict):
        integrity_status = data.get('integrityStatus', None)
        if integrity_status is None:
            integrity = DeviceRiskComponent.Status.NON_EXISTENT
        else:
            if integrity_status.get('isThreatsDetected', False):
                integrity = DeviceRiskComponent.Status.THREAT
            else:
                integrity_status = integrity_status.get('integrity')
                if integrity_status is None:
                    integrity = DeviceRiskComponent.Status.NON_EXISTENT
                elif integrity_status.get('notPassedWhitelisting', 0) > 0:
                    integrity = DeviceRiskComponent.Status.WARNING
                else:
                    integrity = DeviceRiskComponent.Status.PASSED
        return integrity

    @staticmethod
    def _get_firmware_change(data: dict):
        change_status = data.get('changeStatus', None)
        if change_status is None:
            integrity = DeviceRiskComponent.Status.NON_EXISTENT
        else:
            change_status = change_status.get('isChanged', None)
            if change_status is None:
                integrity = DeviceRiskComponent.Status.NON_EXISTENT
            elif change_status:
                integrity = DeviceRiskComponent.Status.WARNING
            else:
                integrity = DeviceRiskComponent.Status.PASSED
        return integrity

    @staticmethod
    def _get_security_risk(data: dict):
        integrity_status = data.get('securityRiskStatus', None)
        if integrity_status is None:
            integrity = DeviceRiskComponent.Status.NON_EXISTENT
        else:
            if integrity_status.get('isSecurityRiskDetected', False):
                integrity = DeviceRiskComponent.Status.WARNING
            else:
                integrity = DeviceRiskComponent.Status.PASSED
        return integrity

    @staticmethod
    def _get_version(data: dict):
        integrity_status = data.get('firmwareVersion', None)
        if integrity_status is None:
            integrity = DeviceRiskComponent.Status.NON_EXISTENT
        else:
            if integrity_status.get('firmwareOutdated', False):
                integrity = DeviceRiskComponent.Status.WARNING
            else:
                integrity = DeviceRiskComponent.Status.PASSED
        return integrity


class DeviceRiskComponents:
    _COMPONENTS_IDS = [
        'BMCFirmware',
        'TPM',
        'PCIDevice',
        'OtherFirmware',
        'NetworkDevices',
        'MeAmt',
        'SystemFirmware',
        'CPU',
        'MBR'
    ]

    def __init__(self, res: List[dict]):
        self.__repr = '{' + ','.join(str(self.set_component(component)) for component in res) + '}'

    def set_component(self, component: dict) -> DeviceRiskComponent:
        name = component['componentName']
        if name not in self._COMPONENTS_IDS:
            raise ValueError(f'unknown component id "{name}"')
        component = DeviceRiskComponent(name, component)
        setattr(self, name, component)
        return component

    def __repr__(self):
        return self.__repr


class _APIInfo:
    def __init__(self, user: str, password: str, domain):
        self.user = user
        self.password = password
        self.domain = domain


class AbstractAPI(ABC):
    def __init__(self, domain: str, session: OAuth2Session):
        self.base_url = domain
        self.session = session

    def _get(self, path: str, print_log: bool = True, **kwargs) -> Response:
        url = self._get_url(path)
        if print_log:
            logging.info('GET %s', url)
        return self.session.get(url=url, **kwargs)

    def _get_json(self, path: str, **kwargs) -> Union[dict, List[dict]]:
        return self._get(path, **kwargs).json()

    def _post(self, path: str, data=None, print_log: bool = True, **kwargs) -> Response:
        url = self._get_url(path)
        if print_log:
            logging.info('POST %s', url)
        return self.session.post(url=url, data=data, **kwargs)

    def _get_url(self, path):
        return urllib.parse.urljoin(self.base_url, path)

    @staticmethod
    @abstractmethod
    def auth(info: _APIInfo) -> 'AbstractAPI':
        pass


class ScannerBackendAPI(AbstractAPI):
    _VERSION = '/api/versions'
    _WHITELIST_STAT = '/api/v1/whitelisting/stats'
    _DASHBOARD_COUNTERS = '/api/v1/dashboard/counters'
    _DEVICE_LIST = '/api/v1/hosttypestatistics'
    _HOST_OUTDATED_STATICS = '/api/v1/hostoutdatedstatistics'
    _TAR_UPLOAD = '/api/v1/bulk/no-metadata-upload'
    _HOSTS_STATIC_INFO = '/api/v1/hoststatistics'
    _FIRMWARE_STATIC_INFO = '/api/v1/firmwarereleasedstatistics?monthsCount=1'
    _SECURITY_DEFINITIONS = '/api/v1/securitytestdefinitions'
    _HOST_DATA = '/api/v1/fullhosts/{host_id}'
    _COMPONENTS_INFO = '/api/v1/hosts/{host_id}/components-info'
    _FILE_UPLOAD = '/api/v1/firmware/simplest-upload'
    _JOB_COUNT = '/api/v1/jobscount'
    _TOKEN = '/api/v1/oauth/admin/token'
    _CLIENT_ID = 'anonymous'
    _CLIENT_SECRET = 'anonymous'
    _HTTPS_PREFIX = 'https://'

    @classmethod
    def auth(cls, info: _APIInfo) -> 'ScannerBackendAPI':
        backend_domain = info.domain if info.domain.startswith(cls._HTTPS_PREFIX) \
                else f'{cls._HTTPS_PREFIX}{info.domain.rstrip("/")}'
        token_url = backend_domain + cls._TOKEN
        logging.info('Log in as %s', info.user)
        auth_request = OAuth2Session(client=LegacyApplicationClient(client_id=cls._CLIENT_ID))
        logging.info('fetching token from %s', token_url)
        auth_request.fetch_token(
            token_url=token_url, username=info.user, password=info.password,
            client_id=cls._CLIENT_ID, client_secret=cls._CLIENT_SECRET)
        return ScannerBackendAPI(backend_domain, auth_request)

    # region COMMON
    def get_version(self) -> str:
        return self._get_json(self._VERSION)['appVersion']

    def get_host_data(self, host, **kwargs: Any) -> dict:
        return self._get_json(self._HOST_DATA.format(host_id=host), **kwargs)

    def get_components(self, host, **kwargs: Any) -> List[dict]:
        return self._get_json(self._COMPONENTS_INFO.format(host_id=host), **kwargs)

    def get_job_count(self, host, **kwargs: Any):
        if host:
            parameters = {'hostId': host}
        else:
            parameters = None
        return self._get_json(self._JOB_COUNT, params=parameters, **kwargs)['count']
    # endregion

    # region DASHBOARD
    def get_dashboard_counters(self) -> dict:
        return self._get_json(self._DASHBOARD_COUNTERS)
    # endregion

    # region vulnerabilities
    def get_vulnerabilities_list(self) -> List[dict]:
        return self._get_json(self._SECURITY_DEFINITIONS)
    # endregion
    # endregion

    # region INTEGRITY
    # region known threats
    def get_device_integrity_info(self) -> DeviceIntegrityInfo:
        data = self._get_json(self._HOSTS_STATIC_INFO)[0]
        integrity = data['integrityCheck']
        return DeviceIntegrityInfo(
            implants=data['activeThreats'],
            devices_checked=integrity['passed'] + integrity['failed'] + integrity['noFirmware'],
            integrity_warnings=integrity['failed'],
            firmware_changes=self._get_json(self._FIRMWARE_STATIC_INFO)[0]['releasedSystemsCount']
        )
    # endregion
    # endregion

    def get_whitelist_stats(self) -> dict:
        return self._get_json(self._WHITELIST_STAT)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Wait for the Eclypsium scan to complete and report results')

    parser.add_argument('-u', '--user', required=True, help='Analytics server username. Example: -u admin')
    parser.add_argument('-p', '--password', required=True, help='Analytics server password. Example: -p <password>')
    parser.add_argument('-s', '--server', required=True, help='Analytics server FQDN. Example: -s prod123.eclypsium.cloud')
    parser.add_argument('-i', '--id', required=True, help='Id for the host that is being scanned. Example: -i 5e165e2d9e12cb00000000abc')
    parser.add_argument('-t', '--timeout', default=120, help='Optional time-out for how long to wait for the job to complete in seconds. Defaults to 120')

    args, unknown_args = parser.parse_known_args()

    host_id = args.id

    apiInfo = _APIInfo(args.user, args.password, args.server)

    api = ScannerBackendAPI.auth(apiInfo)

    print('Waiting for scan results.', end='')
    host_is_not_ready = api.get_job_count(host_id)
    timer = 0
    spinner = itertools.cycle(['-', '/', '|', '\\'])
    while host_is_not_ready:
        sys.stdout.write(next(spinner))
        time.sleep(1)
        timer += 1
        if time > args.timeout:
            print("Timed out waiting for the scan to complete")
            sys.exit(126)
        sys.stdout.flush()                # flush stdout buffer (actual character display)
        sys.stdout.write('\b')            # erase the last written char
        host_is_not_ready = api.get_job_count(host_id)
    print('\nScan results ready:')

    components = DeviceRiskComponents(api.get_components(host_id))
    # print full report for reference
    pprint.pprint(json.loads(str(components)))
    # sample contents of the components result:
    #
    # {'CPU': {'firmware_change': 'NON_EXISTENT',
    #          'integrity': 'NON_EXISTENT',
    #          'security_risk': 'WARNING',
    #          'version': 'PASSED'},
    #  'MBR': {'firmware_change': 'PASSED',
    #          'integrity': 'NON_EXISTENT',
    #          'security_risk': 'NON_EXISTENT',
    #          'version': 'NON_EXISTENT'},
    #  'MeAmt': {'firmware_change': 'PASSED',
    #            'integrity': 'NON_EXISTENT',
    #            'security_risk': 'PASSED',
    #            'version': 'NON_EXISTENT'},
    #  'NetworkDevices': {'firmware_change': 'WARNING',
    #                     'integrity': 'NON_EXISTENT',
    #                     'security_risk': 'NON_EXISTENT',
    #                     'version': 'NON_EXISTENT'},
    #  'OtherFirmware': {'firmware_change': 'PASSED',
    #                    'integrity': 'PASSED',
    #                    'security_risk': 'NON_EXISTENT',
    #                    'version': 'NON_EXISTENT'},
    #  'PCIDevice': {'firmware_change': 'WARNING',
    #                'integrity': 'NON_EXISTENT',
    #                'security_risk': 'NON_EXISTENT',
    #                'version': 'NON_EXISTENT'},
    #  'SystemFirmware': {'firmware_change': 'WARNING',
    #                     'integrity': 'PASSED',
    #                     'security_risk': 'PASSED',
    #                     'version': 'WARNING'},
    #  'TPM': {'firmware_change': 'WARNING',
    #          'integrity': 'NON_EXISTENT',
    #          'security_risk': 'PASSED',
    #          'version': 'NON_EXISTENT'}}
    #
    # This is where we can validate specifis and provide appropriate response.
    # Below is an example

    firmware_results = components.SystemFirmware

    if firmware_results.firmware_change != DeviceRiskComponent.Status.PASSED:
        print("Firmware changed")
        sys.exit(2)
        # or you could
        #  raise ValueError("Failed Firmware change")
